export class Attention {
  id: string;
  code: string;
  name: string;

  constructor(jsonData) {
    Object.assign(this, jsonData);
  }
}
