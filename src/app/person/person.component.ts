import {Component, NgZone, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {Attention} from '../model/attention';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit, OnDestroy {
  event: Observable<Attention>;
  myObservable: Attention;
  subscriptionMyObservable: Subscription = null;

  private events: BehaviorSubject<Attention> = new BehaviorSubject<Attention>(null); // 1
  private eventSource: EventSource;

  constructor(
    private zone: NgZone) { // nuevo !!
  }

  ngOnInit() {
    console.log('hola pe');
    this.eventSource = this.createEventSource();
    this.event = this.createEventObservable(); // observer attention 1

    const observable = {
      next: attention => this.myObservable = attention,
      error: err => console.log,
      complete: () => this.destruir()
    };

    this.subscriptionMyObservable = this.event.subscribe(
      observable
    );
  }

  ngOnDestroy(): void {
    this.subscriptionMyObservable.unsubscribe();
  }

  private createEventObservable(): Observable<Attention> {
    return this.events.asObservable();
  }

  destruir(){
    console.log('destruir');
    this.subscriptionMyObservable.unsubscribe();
  }

  private createEventSource(): EventSource {
    const eventSource = new
    EventSource('http://localhost:8091/case/getGroupAttention?thematicId=5d4316c6fbbc743d8937347b');
    eventSource.onmessage = sse => {
      const event: Attention = new Attention(JSON.parse(sse.data)); // esto es un texto
      this.zone.run(() => this.events.next(event)); // creando un evento
    };
    eventSource.onerror = ev => {
      console.log('weeoe data');
      this.destruirConexion();
    };
    return eventSource;
  }

  destruirConexion() {
    this.eventSource.close();
  }

}
